include .env
.DEFAULT_GOAL := default

clean:
	rm -rf public assets

build:
	./generator/build

ipfs2assets:
	./generator/scripts/ipfs_to_assets.sh

sync:
	./generator/scripts/sync_assets.sh

deploy:
	rsync -az --progress public/ ${REMOTE_HOST}:${REMOTE_DIR}

serve:
	python -m http.server --directory public/

default:
	make clean ipfs2assets sync build
